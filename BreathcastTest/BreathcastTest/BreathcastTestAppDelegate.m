//
//  BreathcastTestAppDelegate.m
//  BreathcastTest
//
//  Created by Jim Zheng on 1/25/12.
//  Copyright 2012 Stanford University. All rights reserved.
//

#import "BreathcastTestAppDelegate.h"
#import "ppDatabaseManager.h"

@implementation BreathcastTestAppDelegate

@synthesize window;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    /*
     * Sample demonstration of how to use this API
     * Very little/no setup necessary
     */
    
    ppUser * tom_user= [[ppUser alloc] initUserWithName:@"TomArnold"
                                       withResting_Rate:21.0 
                                                withUrl:@"http://www.urlToYour.com/photo.jpg"
                                             withPoints:0 withEmail:@"tomAR@gmail.com"]];
    [ppDatabaseManager addMessageForUser:tom_user messages:@"hi tom!"];
    [ppDatabaseManager getMessagesForUser:tom_user]; // returns NSDictionary of [ "hi tom!" : date-it-was-created-as-NSDate ]
    
    // change a property, can sync the user with remote site
    
    [tom_user setEmail:@"changedmeail@gmail.com"];
    [ppDatabaseManager syncUser:tom_user];
    
    // read the docs for more ... 
    
    [tom_user release];
}

@end
