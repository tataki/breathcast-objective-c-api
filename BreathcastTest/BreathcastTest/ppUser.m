//
//  ppUser.m
//  Test
//
//  Created by Jim Z Zheng on 7/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ppUser.h"


@implementation ppUser

@synthesize username;
@synthesize email;
@synthesize image;
@synthesize resting_rate;
@synthesize url;
@synthesize points;
@synthesize last_logged_in;

-(NSData *)imageForURL:(NSString *)thisURL
{	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:thisURL]]; 
	[request setHTTPMethod: @"GET"];
	NSData *responseData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    [request release];
	return responseData;
}

-(ppUser *)initUserWithName:(NSString *)_name
           withResting_Rate:(CGFloat)_rr
                    withUrl:(NSString *)_url
				 withPoints:(CGFloat)_p
                  withEmail:(NSString*)_email
{
	self.username = _name;
	self.resting_rate = _rr;
	self.url = _url;
	self.points = _p;
	self.image = [self imageForURL:self.url];
    self.email = _email;
	return [self autorelease];
}

// place in other checks here for the minimum number of info a user 
// must have
-(BOOL)meetsMinRequirements
{
    return (self.username != NO && self.url != NO && self.email != NO);
}

+(void)logUserData:(ppUser *)user {
    NSDateFormatter* formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    // NSString *str=[formatter stringFromDate:user.last_logged_in];
    
    NSLog(@"\n{\n username:%@\n email:%@\n url:%@\n resting_rate:%g\n points:%g\n last_logged_in:%@}", 
		  user.username,
          user.email,
          user.url,
		  user.resting_rate,
          user.points,
          user.last_logged_in);
}

@end
