//
//  Requests.m
//  BreathcastTest
//
//  Created by Jim Zheng on 1/26/12.
//  Copyright 2012 Stanford University. All rights reserved.
//

#import "Requests.h"

@implementation Requests

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

/* sending POST data in binary form. For sending string data (more common), see the below method 
 * sendHTTPRequest
 * @params paramsArray: an array of NSStrings specifying parameters for the request
 * @param unique: a string unique to this request; used for naming files in the remote location
 */

+(NSString *)sendHTTPPostData:(NSData *)data
                toDestination:(NSString *)url
                   withParams:(NSDictionary *)paramsArray
                   withUnique:(NSString*)unique

{
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];                                    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    // boundary is a string which should not be a part of the data
    NSString *boundary = [NSString stringWithString:@"0xKhTmLbOuNdArY"];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in paramsArray) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [paramsArray objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // add image data
    NSData *imageData = data;
    NSString *FileParamConstant = [NSString stringWithFormat:@"%@.%@", @"uploadedFile", unique];
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        // dont know why this line is needed
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithString:@"Content-Type: image/jpeg\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set URL
    [request setURL:[NSURL URLWithString: url]];
    // make the connection to the web
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    [request release];
    return [returnString autorelease];
}


/* wrapper function on top of NSURL that posts data to remote server.
 * this function takes what's set up between the remote server and the current application
 * and transmits and handles binary data over requests following HTTP standards. 
 *
 * @param type must be @"POST" or @"GET" 
 * Note however note that this only really posts data onto the server
 * 
 * @param uploadURL is the complete url to which the information is to be uploaded not including query strings
 * @param paramString is a string formatted for uploading
 * @param data is any additional data which should be held in the request body
 *
 * the remote source must be designed to return a "0" if execution succeeds, and an error msg upon failure
 * Returns "YES" on success 
 *
 */
+(NSString *)sendHTTPRequest:(NSString *)type 
			   toDestination:(NSString *)uploadURL
			 withParamString:(NSString *)paramString 
					withBody:(NSString *)dataStr
{
	// just adds the data as a last field to the paramstring
	NSString *newParamString;
	if(dataStr) {
		newParamString = [paramString stringByAppendingFormat:@"&data=%@", dataStr];
	}
	else newParamString = paramString;
	
	// init a URL request and encode the param strings in the body
	NSData *requestData = [NSData dataWithBytes: [newParamString UTF8String] length: [newParamString length]];
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: uploadURL]]; 
	[request setHTTPMethod: type];
	[request setHTTPBody: requestData];
	NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
	NSString* newStr = [[NSString alloc] initWithData:returnData
											 encoding:NSUTF8StringEncoding];
    [request autorelease];
	return [newStr autorelease];
}


@end
