//
//  ppDatabaseConfig.h
//  BreathcastTest
//
//  Created by Jim Zheng on 1/25/12.
//  Copyright 2012 Stanford University. All rights reserved.
//

#ifndef BreathcastTest_ppDatabaseConfig_h
#define BreathcastTest_ppDatabaseConfig_h

/*
 * Configurations for Database
 */

#define ROOT @"http://localhost:8000/Breathcast/"
#define UPLOAD_PATH @"upload"
#define DOWNLOAD_PATH @"fetch"

/* replace this with given auth key if using OAuth2; can't perform upload otherwise 
 * if this is given already populated, then this is the key 
 */
#define UPLOAD_SECRET_KEY @""
#define UPLOAD_ACCESS_KEY @""


#endif
