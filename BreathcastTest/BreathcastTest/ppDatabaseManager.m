//
//  ppDatabaseManager.m
//  peripheralpacing
//
//  Created by Jim Zheng on 6/19/11.
//  Copyright 2011 Stanford University. All rights reserved.
//

/* ---------------------------------------------------------------
 
   List of Changes
  
   > Changed the format of the returning dictionary for getMessages
   > and getBPMData. These NSArrays store entries flipped:
   > 
   > [ { 'Message' : 'Timestamp'}, ... ]
   > [ { 'Data' : 'Timestamp'}, ... ]
  
 
   > Created new dependency. For requests, this file depends upon
   > Requests.h to make the actual NSURLRequests.
   >
   > #defines for app configurations are stored within 
   > ppDatabaseConfig.h.
   
 
   > Refactored code so that a lot more of it is now shared between
   > methods.
  
  
  
   -------------------------------------------------------------- */

#import "ppDatabaseManager.h"
#import "SBJson.h"
#import "Requests.h"
#import "ppDatabaseConfig.h"

#define DEFAULT_BPM 0
#define DEFAULT_REST_RATE 13.0
#define MEGABYTE (1024*1024)

// POST-parameter Keywords

#define TYPE_USER @"user"                   // Exact param values to append to key 
#define TYPE_MESSAGE @"message"             // 'type' during a POST upload
#define TYPE_DATA @"data"
#define TYPE_FRIENDSHIP @"friendship"

#define ALL_USER_KW @"__ALL__"              // Query for all objects


// Request Statuses 


#define SUCCESS @"1"                        // Reported request status translation from server
#define FAIL @"2"
#define PARTIAL_FAIL @"3"

@interface ppDatabaseManager()

+(BOOL)handleError:(NSString*)e;
+(NSString *)getUploadURL;
+(NSString *)getDownloadURL;
+(void)Logger:(NSString *)errorString;

+(NSString *)fetchInfoForUser:(NSString *)username 
                     infoType:(NSString *)type 
    withAdditionalParamString:(NSString *)name;

@end

@implementation ppDatabaseManager

+(void)Logger:(NSString *)errorString {
    NSString *file_path = [NSString stringWithString:@"errors.txt"]; 
    [errorString writeToFile:file_path atomically:YES encoding:NSUnicodeStringEncoding error:nil]; 
}

+(NSString *)getUploadURL
{
	NSString *rootpath = ROOT;
	NSString *subpath = UPLOAD_PATH;
	return [rootpath stringByAppendingString:subpath];
}

+(NSString *)getDownloadURL
{
	NSString *rootpath = ROOT;
	NSString *subpath = DOWNLOAD_PATH;
	return [rootpath stringByAppendingString:subpath];
}

+(BOOL)handleError:(NSString*)e 
{
    [e retain];
    BOOL result;
    if([e isEqualToString:SUCCESS]) result = YES;
	else {
		NSLog(@"ERROR: %@", e);
		result = NO;
	}
    [e release];
    return result;
}

#pragma mark - Upload Code Starts Here

/* NSString *configureUploadString
 * ------------------------------
 * Equip the url parameters wiht access/secret key and type support.
 * All functions that do uploading of any kind should use this string to finish 
 * configuring their param strings.
 */
+(NSString *)configureUploadString:(NSString *)uploadStr type:(NSString *)type
{
    NSString *access_key = UPLOAD_ACCESS_KEY;
    NSString *secret_key = UPLOAD_SECRET_KEY;
    return [uploadStr stringByAppendingFormat:@"&type=%@&access_key=%@&secret_key=%@", 
                                        type,
                                        access_key,
                                        secret_key];
}

// Syncs this user with the database
+(BOOL)syncUser:(ppUser *)user
{
    if(!user || ![user meetsMinRequirements]) {
        NSLog(@"ppUser passed into syncUser does not meet min requirements");
        return NO;
    }
    
    NSString *paramStr = [NSString stringWithFormat:@"&username=%@&url=%@&resting_rate=%g&email=%@&points=%d",
                          user.username,
                          user.url,
                          user.resting_rate,
                          user.email,
                          user.points];
    
    paramStr = [self configureUploadString:paramStr type:TYPE_USER];
    
    NSString *result = [Requests sendHTTPRequest:@"POST" 
                                   toDestination:[self getUploadURL] 
                                 withParamString:paramStr 
                                        withBody:NO];
    
	return [self handleError:result];
}

// adds one user by name to the other friend
+(BOOL)addFriendForUser:(ppUser *)from_user newFriend:(ppUser *)to_user
{
	NSString *paramStr = [NSString stringWithFormat:@"&username=%@&from_user=%@&to_user=%@",
                          [from_user username],
                          [from_user username],
                          [to_user username]];
    
    paramStr = [self configureUploadString:paramStr type:TYPE_FRIENDSHIP];
    
	NSString *result = [Requests sendHTTPRequest:@"POST" toDestination:[self getUploadURL] withParamString:paramStr withBody:NO];
	return [self handleError:result];
}

// uploading BPM data means storing the data as as massive array of integers 
+(BOOL)addBPMData:(NSArray *)bpmData forUser:(ppUser *)user
{
	NSString *paramStr = [NSString stringWithFormat:@"&username=%@",
						  [user username]];
    paramStr = [self configureUploadString:paramStr type:TYPE_DATA];
    

	SBJsonWriter *writer = [[SBJsonWriter alloc] init];
    
	// we place the data in proper dictionary json format
	// so that it can be more easily dissected by the backend.
	// this does take up more space if there are lots of data points.
    
	/*NSMutableDictionary *dictRepresentation = [[NSMutableDictionary alloc] init];
	for(int j = 0; j < [bpmData count]; j++) {
		[dictRepresentation setObject:[bpmData objectAtIndex:j] forKey:[NSString stringWithFormat:@"%d", 1 + j]];
	}*/
	NSString *body = [writer stringWithObject:bpmData]; 
    //[dictRepresentation release];
	[writer release];
	NSString *result = [Requests sendHTTPRequest:@"POST" toDestination:[self getUploadURL] withParamString:paramStr withBody:body];
	return [self handleError:result];
}

// Inserts a new message for the user
+(BOOL)addMessageForUser:(ppUser *)user messages:(NSString *)newMsg 
{
	NSString *paramStr = [NSString stringWithFormat:@"username=%@&message=%@",
						  [user username],
						  newMsg];
    paramStr = [self configureUploadString:paramStr type:TYPE_MESSAGE];
	NSString *result= [Requests sendHTTPRequest:@"POST" toDestination:[self getUploadURL] withParamString:paramStr withBody:NO];
	return [self handleError:result];	
}

// Update the current timestamp for the user
+(BOOL)userLoggedIn:(NSString *)name 
{
    NSString *paramStr = [NSString stringWithFormat:@"username=%@&last_logged_in=%d",
						  name,
						  0];
    paramStr = [self configureUploadString:paramStr type:TYPE_USER];
	NSString *result= [Requests sendHTTPRequest:@"POST" toDestination:[self getUploadURL] withParamString:paramStr withBody:NO];
	return [self handleError:result];
}

#pragma mark - Fetch Code Starts Here

/*
 * NSString *fetchInfoForUser:
 * -----------------------------
 * Takes care of logic for fetching user info
 * from the database
 * 
 * all logic for fetching functions go in here. 
 * We don't know how the data is going to be formatted so we 
 * can just pass back the response as an array.
 * Let the receiver deal with memory management
 * 
 */
+(NSString *)fetchInfoForUser:(NSString *)username infoType:(NSString *)type withAdditionalParamString:(NSString *)toAppend {
    NSString *paramStr = [NSString stringWithFormat:@"&username=%@",
                          username];
    paramStr = [paramStr stringByAppendingFormat:@"&type=%@", type];
    if(toAppend) {
        paramStr = [paramStr stringByAppendingString:toAppend];
    }
    return [Requests sendHTTPRequest:@"POST" 
                                   toDestination:[self getDownloadURL] 
                                 withParamString:paramStr withBody:NO];
}

// returns an NSArray whose entries are NSDictionaries of the format
// {"bpm" : 23.4 , "timestamp", 2011-12-41 01:23}
+(NSArray *)getBPMDataForUser:(ppUser *)user
{
	NSString *returnStr = [self fetchInfoForUser:[user username] infoType:TYPE_DATA
                       withAdditionalParamString:NO];
    if(!returnStr) return NULL;
    
	// parse the formatted data returned into bpm data, which should
	// be properly formatted already
	SBJsonParser *parser = [[SBJsonParser alloc] init];
	NSArray *result = [parser objectWithString:returnStr];
	[parser release];
	return result;	
}

// converts Unix timestamp-converted date in PHP/Python/Ruby into NSDate.
// Date is of the specific form, e.g. '2011-08-11 17:12', denoting 08/11/2011 at 5:12PM.
+(NSDate *)UnixDateToNSDate:(NSString *)toConvert {
	[NSDateFormatter setDefaultFormatterBehavior:NSDateFormatterBehaviorDefault];
	NSDateFormatter *dF = [[NSDateFormatter alloc] init];
    [dF setDateFormat:@"yyyy-MM-dd HH:mm:SS"];
	[dF autorelease];
    return [dF dateFromString:toConvert];
}

// converts a dictionary object to a user object
+(ppUser *)dictionarytoUser:(NSDictionary *)dict withUserName:(NSString *)username {
    ppUser *newUser = [[ppUser alloc] init];
    [newUser setResting_rate:[[dict objectForKey:@"resting_rate"] floatValue]];

    [newUser setUsername:username];
    [newUser setEmail:[dict objectForKey:@"email"]];
    [newUser setUrl:[dict objectForKey:@"url"]];
    [newUser setPoints:[[dict objectForKey:@"points"] integerValue]];
    [newUser setLast_logged_in:[self UnixDateToNSDate:[dict objectForKey:@"last_logged_in"]]];
    return [newUser autorelease];
}

// converts specifically formatted json with appropriate user information into 
// a ppUser, allocates space for that user, and returns a pointer to the new user
+(ppUser *)jsonToUser:(NSString *)info
{
	SBJsonParser *parser = [[SBJsonParser alloc] init];
	NSDictionary *userInfo = [parser objectWithString:info error:nil];
	if(!userInfo) {
		NSLog(@"Error; cannot parse user data in jsontoUser:(NSString *)info");
        [userInfo release];
        [parser release];
		return NULL;
	}
    [parser release];
    return [self dictionarytoUser:userInfo withUserName:[userInfo objectForKey:@"username"]];
}

// gets the user info stored remotely; response comes in the form of
// JSON string to be parsed into a dictionary. JSON formatted according
// to PHP standards.
+(ppUser *)getUserInfo:(NSString *)name
{
    NSString *returnStr = [self fetchInfoForUser:name infoType:TYPE_USER 
                       withAdditionalParamString:NO];
    if(!returnStr) return NULL;
	return [self jsonToUser:returnStr];
}

// returns autoreleased array of dictionaries that represent
// users
+(NSDictionary *)getFriendsOf:(ppUser *)user
{
	NSString *returnStr = [self fetchInfoForUser:[user username] infoType:TYPE_FRIENDSHIP 
                       withAdditionalParamString:NO];
    if(!returnStr) return NULL;
    
	SBJsonParser *parser = [[SBJsonParser alloc] init];
   
    
	NSDictionary *friendDict = [parser objectWithString:returnStr];
	[parser release];
    
    // friendDict is a dict of { NSString names :  NSDictionary of user property key/values }
    // e.g { "ken_jennings" : { points : 23, age : 33 ... }
    //       }
    // we turn this into dictionaryUses
	
	NSMutableArray *result = [[NSMutableArray alloc] init];
	for(id key in friendDict) {
		[result addObject:[self dictionarytoUser:[friendDict objectForKey:key] 
                                    withUserName:(NSString *)key]];
	}
	
	return [result autorelease];
	
}

// gets info for all users. Returns an NSArray of ppUsers
// Shouldn't have to call this more than once; really gets
// ALL users in the backend 
+(NSArray *)getAllUsers 
{
	NSString *returnStr = [self fetchInfoForUser:ALL_USER_KW
                                        infoType:TYPE_USER 
                       withAdditionalParamString:NO];
    
    if(!returnStr) return NULL;
    
    SBJsonParser *parser = [[SBJsonParser alloc] init];
    
    // result contains NSArray of NSDictionaries, each 
    // dictionary corresponding to a user's information
    NSDictionary *allUserDict = [parser objectWithString:returnStr];
    [parser release];
    
    NSMutableArray *allUsers = [[NSMutableArray alloc] init];
    for (NSString *key in allUserDict) {
        [allUsers addObject:[self dictionarytoUser:[allUserDict objectForKey:key]
                                      withUserName:key]];
    }
    
    // cleanup
	return [allUsers autorelease];
}

/*
 * Gets JSON string of ["Message" => "Date"]
 * Returns an NSArray of NSDictionaries, each entry of ["Message" => "Date"]
 * 
 */
+(NSDictionary *)getMessagesForUser:(ppUser *)user
{
    NSString *returnStr = [self fetchInfoForUser:[user username] infoType:TYPE_MESSAGE 
                                                withAdditionalParamString:NO];
    if(!returnStr) return NULL;
    
    SBJsonParser *parser = [[SBJsonParser alloc] init];
    NSDictionary *rawInfo = [parser objectWithString:returnStr];

    NSMutableDictionary *dictToReturn = [[NSMutableDictionary alloc] init];
    NSEnumerator *enumerator = [rawInfo keyEnumerator];
    id key;
    while (key = [enumerator nextObject]) {
        NSDate *convertedDate = [self UnixDateToNSDate:[rawInfo objectForKey:key]];
        [dictToReturn setObject:convertedDate forKey:key];
    }
    [parser release];
    return [dictToReturn autorelease];
}

@end

