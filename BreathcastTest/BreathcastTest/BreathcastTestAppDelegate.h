//
//  BreathcastTestAppDelegate.h
//  BreathcastTest
//
//  Created by Jim Zheng on 1/25/12.
//  Copyright 2012 Stanford University. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface BreathcastTestAppDelegate : NSObject <NSApplicationDelegate> {
    NSWindow *window;
}

@property (assign) IBOutlet NSWindow *window;

@end
