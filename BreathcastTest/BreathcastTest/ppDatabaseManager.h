//
//  ppDatabaseManager.h
//  peripheralpacing
//
//  Created by Jim Zheng on 6/19/11.
//  Copyright 2011 Stanford University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ppUser.h"
#import "SBJsonParser.h"
#import "SBJsonWriter.h"
#import "ppDatabaseConfig.h"

@interface ppDatabaseManager : NSObject

/* Path configurations for remote server;
 * TODO: 
 *   Change these configurations manually
 *   if the site changes, 
 */

/* DATABASE API
 * 
 * Encapsulates access and manipulation of information 
 * stored remotely on the Stanford server.
 * Transforms any given dataform into JSON, then 
 * uploads it to the URL specified by appending
 * to ROOT either the UPLOAD or DOWNLOAD path 
 * 
 * 
 */

#pragma mark - Uploading

/* BOOL syncUser
 *
 * Requires that all fields are present, otherwise this function quits and does nothing. 
 * Syncs the given field to remote storage. Can be used for both creating and upating
 * information about users.
 *
 */
+(BOOL)syncUser:(ppUser *)user;

/* BOOL addMessageForUser
 * 
 * Adds a single string message for the user. In the future, may want to adress
 * possibility of adding additional information along with this message.
 */
+(BOOL)addMessageForUser:(ppUser *)user messages:(NSString *)message;

/* BOOL addFriendForUser
 * 
 * Adds a one-way relation between from_user to to_user. 
 * This friendship is not yet confirmed however, and an attempt
 * to fetch friend information after this call will not necessarily
 * result in this relationsihp being present, unless to_user has 
 * confirmed from_user. 
 */
+(BOOL)addFriendForUser:(ppUser *)from_user newFriend:(ppUser *)to_user;

/* BOOL addBPMData
 *
 * Bulk uploading of data to remote source. 
 * May want to call at varying intervals, since this function may 
 * take some while to return if given a large dataset.
 */
+(BOOL)addBPMData:(NSArray *)bpmData forUser:(ppUser *)user;

/* BOOL userLoggedIn
 *
 * Call every time the user first logs in. 
 * CREATION: If creating user for the first time, no need
 * to call this. Cannot be called on a non-user, will return
 * error. To detect whether a user is in the system, try to 
 * get the user with the specific name. If user does not exist,
 * create the user. Otherwise, call this function.
 */
+(BOOL)userLoggedIn:(ppUser *)user;



#pragma mark - Fetching

/* ppUser *getUserInfo
 * 
 * Returns a ppUser object from info retrieved from the database,
 * or NULL if the user does not exist. Used in coordination with 
 * syncUser to manage user creation.
 */
+(ppUser *)getUserInfo:(NSString *)name; // NOTE: ppUser returned is not retained

+(NSDictionary *)getFriendsOf:(ppUser *)user; // an NSArray of ppUsers
+(NSArray *)getAllUsers; // NSArray containing names (NSStrings) only
+(NSDictionary *)getBPMDataForUser:(ppUser *)user;
+(NSDictionary *)getMessagesForUser:(ppUser *)user; // gets a dictionary of keys as times and valuas 

//+(NSDate *)getLastUpdate:(NSString*)name; // gets the most recent BPM data
//+(NSArray *)getBPMLastDay:(NSString *)name; // gets all BPM data of a cetain day. 


@end
