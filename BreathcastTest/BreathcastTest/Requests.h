//
//  Requests.h
//  BreathcastTest
//
//  Created by Jim Zheng on 1/26/12.
//  Copyright 2012 Stanford University. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 * Methods that facilitate sending / receiving requests to and from the
 * user.
 */

@interface Requests : NSObject

/*
 * used for sending media data using the multipart/form-data boundary header
 */
+(NSString *)sendHTTPPostData:(NSData *)data
                toDestination:(NSString *)url
                   withParams:(NSDictionary *)paramsArray
                   withUnique:(NSString*)unique;

/*
 * used for sending string and UTF8String-encoded data
 */
+(NSString *)sendHTTPRequest:(NSString *)type 
			   toDestination:(NSString *)uploadURL
			 withParamString:(NSString *)paramString 
					withBody:(NSString *)dataStr;

@end
