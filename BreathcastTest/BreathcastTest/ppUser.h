//
//  ppUser.h
//  
//  Created by Jim Z Zheng on 7/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
//  Representative of a typical user. This class handles the 
//  remote interaction of gettting/retrieving data.
//	Usage:
//  Initialize this class using its initUserWithName method
//  and change properties using getters/setters.


#import <Cocoa/Cocoa.h>

// UserColorMode defines how "well" the user is breathing; green = optimal, red = alarming
typedef enum {
    UCMRed,
    UCMGreen,
    UCMYellow
} UserColorMode;

// Common model used for storing information about usrs
// in our remote and local databases. 
@interface ppUser : NSObject {
	NSString *username;
    NSString *email;
	NSData *image;
	CGFloat resting_rate;
	NSString *url; // keep track of url for the user
	CGFloat points;
    NSDate *last_logged_in;
}

@property (nonatomic, retain) NSString *username;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSData *image;
@property (nonatomic) CGFloat resting_rate;
@property (nonatomic, retain) NSString *url;
@property (nonatomic) CGFloat points;
@property (nonatomic, retain) NSDate *last_logged_in;

// gets the NSData for this user based on the stored data
-(NSData *)imageForURL:(NSString *)url;

-(ppUser *)initUserWithName:(NSString *)name
			withResting_Rate:(CGFloat)resting_rate
			   withUrl:(NSString *)url
				 withPoints:(CGFloat)newPoints
                  withEmail:(NSString *)email;

/* makes sure this user meets the minimum information requirement
 * that is, this user has a non-NULL:
 *   > username
 *   > email
 *   > url
 */
-(BOOL)meetsMinRequirements;

// prints out a user's information in easy-to-read form
+(void)logUserData:(ppUser *)user;


@end
