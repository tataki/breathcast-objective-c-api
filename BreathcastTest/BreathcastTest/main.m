//
//  main.m
//  BreathcastTest
//
//  Created by Jim Zheng on 1/25/12.
//  Copyright 2012 Stanford University. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
